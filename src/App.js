import React from 'react'
import NavBar from './components/Header/NavBar'
import Teaser from './components/Header/teaser'
import './App.css'
import Tazeha from './components/Content/Tazeha'
import Advertise from './components/advertise/advertise'
import Footer from './components/Footer/Footer'
import Simorgh from './components/Content/simorgh'
import MajmooeFilmha from './components/Content/MajmooeFilmha'
function App() {
  return (
    <div className="App">
    <div >
       <NavBar/>
      <Teaser/>
       <Advertise/>
    </div>
    <div>
      <Tazeha/>
      <Simorgh/>
      <MajmooeFilmha/>
    
    </div>

    <div className="footer"> 
      <Footer/>
      </div>
   
    </div>
  )
}

export default App


