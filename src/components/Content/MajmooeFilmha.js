import React from 'react'
import './tazeha.css'
import InfiniteCarousel from 'react-leaf-carousel';
import Image from 'react-bootstrap/Image'

const majmooefilmha=require("./MajmooeFilmha.json")
function MajmooeFilmha() {
  

    return (
     <>
     <div style={{marginTop:'100px'}}>
   
     <InfiniteCarousel
    breakpoints={[
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
    ]}
    dots={false}
    showSides={true}
    sidesOpacity={.5}
    sideSize={.1}
    slidesToScroll={3}
    slidesToShow={3}
    scrollOnDevice={true}
  
>
            {majmooefilmha.map(list => (
                <figure>
            <Image  style={{width:'450px', height:'200px' , borderRadius:'5px' }}
             src={require('./majmooefilmha' + list.Image)}  onMouseOver="image"
           />  
            <figcaption>{list.name}</figcaption>
            </figure>
      ))}

  </InfiniteCarousel>
  </div>
</>
    )
}

export default MajmooeFilmha
