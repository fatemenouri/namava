import React , { useState} from 'react'
import './tazeha.css'
import ItemsCarousel from 'react-items-carousel';
import styled from 'styled-components';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import Button from 'react-bootstrap/Button'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import FavoriteIcon from '@material-ui/icons/Favorite';
import ReactPlayer from 'react-player'
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import YouTube from '@u-wave/react-youtube';



const tazeha=require("./tazeha.json")

const Wrapper = styled.div`
{
  "noOfChildren": 10,
  "wrapperStyle": {
    "padding": 0,
    "maxWidth": "100%",
    "margin": "0"
  },
  "componentProps": {
    "infiniteLoop": false,
    "gutter": 12,
    "activePosition": "center",
    "chevronWidth": 60,
    "disableSwipe": false,
    "alwaysShowChevrons": false,
    "numberOfCards": 3,
    "slidesToScroll": 3,
    "outsideChevron": false,
    "showSlither": false,
    "firstAndLastGutter": true
  }
}
`;

function Tazeha() {

 
     const [activeItemIndex, setActiveItemIndex] = useState(0);   
     const [description, setdescription] = useState('desc'); 
     const [visible, setVisible] = useState(false); 
     const [classname , setclassname] = useState(false); 
     const [selected , setSelected] = useState(false); 
    

 const handleClick=(index)=> {

        setdescription(index);
        setSelected(!selected);
        setclassname(!classname);
        setVisible(!(visible));
        console.log(visible)
      
       }

 const filterDropdown = tazeha.filter(function(result) {
          return result.name === description;
        });

    return (
     <>
   
     <h3 className="title">
     <span >تازه های ایرانی</span>
     </h3>
  
  <Container style={{"padding":0,"maxWidth":"100%","margin":"0"}}>
 
     <Wrapper>
      <ItemsCarousel
       infiniteLoop={false}
       gutter={12}
       activePosition={'center'}
       chevronWidth={100}
       disableSwipe={false}
       alwaysShowChevrons={false}
       numberOfCards={7}
       slidesToScroll={7}
       outsideChevron={false}
       showSlither={false}
       firstAndLastGutter={true}
        activeItemIndex={activeItemIndex}
        requestToChangeActive={setActiveItemIndex}
        rightChevron={<ArrowForwardIosIcon id='forward' color='action' fontSize='large'/>}
        leftChevron={<ArrowBackIosIcon   id='back'  color='action' fontSize='large'/>}
      
      >
      
 {tazeha.map(lists => (
            <Row>
              
                <figure style={{margin:'50px'}} >   
              <img    key={lists.name}
             src={require('./tazehaImage' + lists.Image)} alt='tazeha' id='images' onClick={()=>handleClick(lists.name)}
              /> 
             
            <figcaption>{lists.name}</figcaption>
            </figure></Row>
            ))}
  </ItemsCarousel>  </Wrapper> 

   {filterDropdown.map(list=>(


   
    <div id='div' style={{ 'backgroundImage': `url(${require('./tazehaImage' +list.image)})` , color:'white', textAlign:'right'}} sm={12} xs={12} md={12}>
       
          <Row style={{ paddingTop:'200px'}}> 
          <Col xl={{ span: 4, offset: 7 }}>
         
           <FavoriteIcon/>
           {list.like}-
          {list.year} -
          {list.time}
            <Button size='sm' variant="warning" className="disabledbtn" >پردیس نماوا</Button>
           <Button size='sm' variant="warning" className="disabledbtn" >{list.gp}</Button>
           </Col>
          </Row>
          <Row >
            <Col xl={{ span: 4, offset: 7 }}>{list.description}</Col>
            </Row>
          <Row >
            <Col xl={{ span: 4, offset: 7 }}>
            ستارگان:
      {list.actor1}-
      {list.actor2}-
      {list.actor3}
      </Col></Row>
      <Row>
      <Col xl={{ span: 4, offset: 7 }}>
        کارگردان:
        {list.director}
      </Col></Row>
      <Row>
      <Col xl={{ span: 4, offset: 7 }}>
        دسته بندی:
        {list.category}
      </Col></Row>
     <Row>
       <Col xl={{ span: 4, offset: 7 }}>
         
       <Button variant="outline-light" href="تیزر">
         <PlayArrowIcon/>
              پخش فیلم
            </Button>
       </Col>
     </Row>
     <Row>
       <Col  xl={{ span: 4, offset: 7 }}>
        <ReactPlayer url={list.url} width="450px" height='250px'/>
       </Col>
     </Row>
     
</div>
       
))}

</Container>

</>
    )
}

export default Tazeha
