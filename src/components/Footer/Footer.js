import React from 'react'
import Nav from 'react-bootstrap/Nav'
import './footer.css'  
import TelegramIcon from '@material-ui/icons/Telegram';
import InstagramIcon from '@material-ui/icons/Instagram';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import EmailIcon from '@material-ui/icons/Email';
const links=require("./footer.json");
function Footer() {
    return (
        <div>
           
            <Row style={{paddingTop:'100px'}}>
               
           <Col xl={{ span: 4, offset: 7 }}>
               <p>تماس با ما </p>
            <a href='https://t.me/fatemenouri76'><TelegramIcon fontSize='large' className="telegram" /></a>
           
            <a href="https://instagram.com/fatemenourii?igshid=189pekrrrapav"><InstagramIcon  fontSize='large' className="instagram" /></a>
             <p>fatemenouri99@gmail.com<EmailIcon fontSize='large'/></p>
           </Col>
         
         </Row>
      <Row>
      <Col xl={{ span: 4, offset: 4 }}>
            <p>خدمات ارائه شده در نماوا دارای مجوزهای لازم از مراجع مربوط می باشد.وهرگونه بهره برداری و سوء استفاده از آن پیگرد قانونی دارد</p>
            </Col>
            </Row>
      </div>
        
    )
}

export default Footer
