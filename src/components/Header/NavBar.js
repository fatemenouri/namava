import React from 'react'
import './nav.css'
import Navbar from 'react-bootstrap/Navbar'
import FormControl from 'react-bootstrap/FormControl'
import Form from 'react-bootstrap/Form'
import Nav from 'react-bootstrap/Nav'
import Button from 'react-bootstrap/Button'





export default function NavBar(){

    return(

        <Navbar bg="dark" expand="lg" variant="dark">
    
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">  
         <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Button variant="outline-light">Search</Button>
          </Form>
          <Nav className="mr-auto">
         <Button variant="outline-light" href="ورود/ثبت نام">
                ورود/ثبت نام
            </Button>
            <Nav.Link href="#خرید اشتراک">
                خرید اشتراک
            </Nav.Link>
           
          </Nav>
       
        </Navbar.Collapse>   
        <Nav.Link href="#home">خانه</Nav.Link>
         <Navbar.Brand href="#home">نماوا</Navbar.Brand>
      </Navbar>

    );




}