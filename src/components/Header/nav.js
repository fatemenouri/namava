import React from 'react'
import './nav.css'
import Button from 'react-bootstrap/Button'
import IconButton from '@material-ui/core/IconButton';
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import SearchIcon from '@material-ui/icons/Search';
 


export default function NavBar() {
 

  return (
   <div>
    <Navbar bg="none" expand="lg" variant='dark'>
     <Navbar.Toggle aria-controls="basic-navbar-nav" />
     <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="mr-auto">
    
       <Button variant="outline-light"  >ورود/ثبت نام</Button>
        <Nav.Link href="#خرید اشتراک" style={{color:"#ffffff"}} >
           خرید اشتراک
       </Nav.Link>
      <IconButton><SearchIcon  style={{color:"#ffffff"}}/></IconButton>
    </Nav>
      <Nav.Link href="#home">خانه</Nav.Link>
      
       <Navbar.Brand className="font" href="#home">نماوا</Navbar.Brand> 
  </Navbar.Collapse>
</Navbar>
</div>
  );
}
