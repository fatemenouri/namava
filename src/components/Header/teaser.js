import React from 'react';
import { Slide } from 'react-slideshow-image';
import teaser1 from '../../img/teaser1.jpg'
import teaser2 from '../../img/teaser2.jpg'
import teaser3 from '../../img/teaser3.jpg'
import teaser4 from '../../img/teaser4.jpg'
import './teaser.css'
import Button from 'react-bootstrap/Button'
import PlayArrowIcon from '@material-ui/icons/PlayArrow';


const slideImages = [
  teaser1,
  teaser2,
  teaser3,
  teaser4,
];

const properties = {
  duration: 5000,
  transitionDuration: 500,
  infinite: true,
  indicators: false,
  arrows: true,
  pauseOnHover: true,
  onChange: (oldIndex, newIndex) => {
    console.log(`slide transition from ${oldIndex} to ${newIndex}`);
  }
}

function Teaser() {

  return (
    <div>

      <Slide {...properties}>
    
        <div className="each-slide">
          <div style={{ 'backgroundImage': `url(${slideImages[0]})` }}>
            <figure style={{ marginRight: '250px' }}>
              <img style={{ width: '300px', marginLeft: '1200px' }} src={require('../../img/hamgonah.jpg')} alt="هم گناه" />
              <figcaption style={{ fontSize: 'x-large' }}>هم گناه</figcaption>
              <br />
              <figcaption><Button variant="light" >عضویت و دریافت یک رو اشتراک رایگان</Button></figcaption>
            </figure>
          </div>
        </div>
        
        <div className="each-slide">
          <div style={{ 'backgroundImage': `url(${slideImages[1]})` }}>
            <figure>
              <img style={{ width: '300px', marginLeft: '1000px' }} src={require('../../img/خروج.png')} alt="خروج" />
              <div>
                <figcaption style={{ fontSize: 'large' }}>
                  <Button size='sm' variant="warning" className="disabledbtn" >پردیس نماوا</Button>
                 خروج
                  </figcaption>
              </div>
              <br />
              <figcaption><Button variant="light" >خرید بلیت <PlayArrowIcon /></Button></figcaption>
            </figure>
          </div>
        </div>
        <div className="each-slide">
          <div style={{ 'backgroundImage': `url(${slideImages[2]})` }}>
            <figure>
              <img style={{ width: '300px', marginLeft: '1000px' }} src={require('../../img/طلا.png')} alt="طلا"/>
              <div style={{ marginTop: '10px' }}>
                <figcaption style={{ fontSize: 'large' }}>
                  <Button size='sm' variant="warning" className="disabledbtn" >پردیس نماوا</Button>
                    طلا
                  </figcaption>
              </div>
              <br />
              <figcaption><Button variant="light" >خرید بلیت <PlayArrowIcon /></Button></figcaption>
            </figure>
          </div>
        </div>
        <div className="each-slide">
          <div style={{ 'backgroundImage': `url(${slideImages[3]})` }}>
            <figure>
              <img style={{ width: '300px', marginLeft: '1000px' }} src={require('../../img/سمفونی نهم.png')} alt="سمفونی نهم"/>
              <figcaption style={{ fontSize: 'x-large' }}>سمفونی نهم</figcaption>
              <br />
              <figcaption><Button variant="light" >عضویت و دریافت یک رو اشتراک رایگان</Button></figcaption>
            </figure>
          </div>
        </div>
      </Slide>
   
    </div>
  )
}

export default Teaser

