import React from 'react'
import InfiniteCarousel from 'react-leaf-carousel';

const advertise=require("./advertise.json")

function Advertise() {
    return (
     <>
  
     <InfiniteCarousel
    breakpoints={[
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
    ]}
    dots={false}
    showSides={true}
    sidesOpacity={.5}
    sideSize={.1}
    slidesToScroll={3}
    slidesToShow={3}
    scrollOnDevice={true}
  >
  
            {advertise.map(ad => (
                
            <img key={ad.name} style={{width:'460px' , height:'230px' , borderRadius:'2%'}} src={require('./img' + ad.Image)} alt='ad' />  
           
      ))}
    

  </InfiniteCarousel>
</>
    )
}

export default Advertise
